const HtmlWebPackPlugin = require('html-webpack-plugin')
const path = require('path')
const webpack = require('webpack')
const TerserPlugin = require('terser-webpack-plugin');

const htmlPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './index.html',
})
module.exports = {
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
    mergeDuplicateChunks: true,
  },
  entry: './src/App.js',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: '/',
  },
  plugins: [htmlPlugin, new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader'
        }, {
          loader: 'eslint-loader',
          options: { }
        }]
      },
      {
        test: /\.(png|svg|jpg|gif|ttf)$/,
        loader: 'file-loader',
        options: { name: '/static/[name].[ext]' },
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
           'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [
                require('tailwindcss'),
                require('autoprefixer'),
              ],
            },
          },
        ],
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
  },
  //devtool: 'cheap-module-eval-source-map',
}
