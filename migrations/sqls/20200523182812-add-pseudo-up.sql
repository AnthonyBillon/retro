alter table vote add column pseudo varchar;
update vote set pseudo='developpeur perdu' where pseudo is null;
alter table vote alter column pseudo set not null;