create table if not exists votesession(
	id serial primary key,
	code varchar (6) unique not null
);

create table if not exists vote(
	id serial primary key,
	votesession_id integer not null,
	r integer,
	d integer,
	a integer,
	q integer,
	couleur varchar(7) not null,
	voteId uuid unique,
	foreign key (votesession_id) references votesession(id)
);