echo "Le build de Erdak a commencé"
npm install
npm run build

echo "nettoyage..."
rm -rf src
rm -rf .eslintrc.js
rm .eslintignore
rm tailwind.config.js
rm webpack.config.js

npm ci --only=production
