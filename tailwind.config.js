module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.js',
  ],
  theme: {
    extend: {
      colors: {
        primary: 'var(--bg-background-primary)',
        secondary: 'var(--bg-background-primary)',
        ternary: 'var(--bg-background-primary)',
        accent: 'var(--accent-color)',
      },
    },
  },
  variants: {},
  plugins: [],
}
