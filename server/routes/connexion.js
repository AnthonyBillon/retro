const express = require('express')

const router = express.Router()
const controller = require('../controllers/connexion')

router.post('/create', controller.createSession)
router.post('/', controller.connexion)

module.exports = router
