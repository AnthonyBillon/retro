const express = require('express')

const router = express.Router()
const controller = require('../controllers/votes')
const auth = require('../middleware/auth')

router.get('/', auth, controller.votes)

module.exports = router
