const socketIo = require('socket.io')
const jsonwebtoken = require('jsonwebtoken')
const logger = require('../util/logger')
const voteService = require('../services/vote')

const io = socketIo()

const emitError = (client, error) => {
  client.emit('ERROR', {
    error,
  })
}

io.on('connection', async (client) => {
  logger.info('connexion client')
  client.on('join', (room) => {
    logger.info(`un utilisateur a rejoint ${room}`)
    client.join(room)
  })

  client.on('ADD_DATA', ({
    R, D, A, Q, jwt,
  }) => {
    jsonwebtoken.verify(jwt, 'secret', async (err, decoded) => {
      if (err) {
        logger.info('Token rejetté')
        emitError(client, 'Le token est invalide')
      } else {
        logger.info('vote reçu')
        try {
          await voteService.addVote(decoded.id, {
            R,
            D,
            A,
            Q,
          })


          const vote = await voteService.getVote(decoded.id)

          io.sockets.in(decoded.code).emit('ADD_DATA', {
            R,
            D,
            A,
            Q,
            voteid: decoded.id,
            couleur: vote.couleur,
            pseudo: vote.pseudo,
          })
        } catch (e) {
          logger.error(e)
          emitError(client, "L'ajout du vote a échoué")
        }
      }
    })
  })

  client.on('leave', () => {
    client.leaveAll()
  })
})

module.exports = { io }
