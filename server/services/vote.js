const jsonwebtoken = require('jsonwebtoken')
const uuid = require('uuid')
const colorCreator = require('../util/colorcreator')
const codegen = require('../util/votecodegenerator')
const pseudogen = require('../util/pseudogenerator')
const queries = require('../queries/vote')
const errors = require('../util/error')
const logger = require('../util/logger')

const createSession = async () => {
  try {
    logger.info('creation de la session')
    const code = codegen()
    await queries.createSession(code)

    const jwt = await jsonwebtoken.sign({
      code,
      role: 'host',
    }, 'secret')
    logger.info(`création de la session ${code}`)
    return { jwt, code }
  } catch (e) {
    logger.error(JSON.stringify(e))
    throw errors.sessionCreationError
  }
}

const connectSession = async (code) => {
  try {
    logger.info('connexion à la session')
    const color = colorCreator()
    const pseudo = pseudogen()
    const result = await queries.getSessionIdByCode(code)

    if (result.rows.length === 0) {
      throw errors.sessionInexistante
    }
    const sessionId = result.rows[0].id

    const id = uuid.v4()
    await queries.connectSession(color, sessionId, id, pseudo)

    const jwt = await jsonwebtoken.sign({
      code,
      role: 'client',
      id,
    }, 'secret')

    logger.info(`Connexion à la session ${code}`)
    return {
      jwt,
      color,
      pseudo,
      voteid: id,
    }
  } catch (e) {
    logger.error(JSON.stringify(e))
    if (e.errno) {
      throw e
    } else {
      throw errors.sessionJoinError
    }
  }
}

const addVote = async (id, vote) => {
  try {
    await queries.addVote(id, vote)
  } catch (e) {
    logger.error(JSON.stringify(e))
    throw errors.voteAddError
  }
}

const getVotes = async (code) => {
  try {
    const result = await queries.getVotes(code)
    return result.rows
  } catch (e) {
    logger.error(JSON.stringify(e))
    throw errors.getVotesError
  }
}

const getVote = async (voteId) => {
  try {
    const result = await queries.getVote(voteId)
    if (result.rows.length === 1) {
      return result.rows[0]
    }
    logger.error('Vote non trouvé')
    throw new Error(errors.getVotesError)
  } catch (e) {
    logger.error(JSON.stringify(e))
    throw errors.getVotesError
  }
}

module.exports = {
  createSession,
  connectSession,
  addVote,
  getVotes,
  getVote,
}
