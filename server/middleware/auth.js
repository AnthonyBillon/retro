const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers.authorization
  // Express headers are auto converted to lowercase
  if (token.startsWith('Bearer ')) {
    // Remove Bearer from string
    token = token.slice(7, token.length).trimLeft()
  }
  if (token) {
    jwt.verify(token, 'secret', (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid',
        })
      }
      req.decoded = decoded
      next()
      return undefined
    })
  }
}
