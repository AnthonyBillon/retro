const codegen = () => {
  const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ'
  let randomString = ''
  for (let i = 0; i < 6; i += 1) {
    const char = Math.floor(Math.random() * chars.length)
    randomString += chars.substring(char, char + 1)
  }
  return randomString
}

module.exports = codegen
