const errors = {
  sessionCreationError: {
    errno: 100,
    text: 'La création de la session a échoué',
  },
  sessionInexistante: {
    errno: 101,
    text: 'La session n\'existe pas',
  },
  sessionJoinError: {
    errno: 102,
    text: 'Impossible de rejoindre la session',
  },
  voteAddError: {
    errno: 200,
    text: 'L\'ajout du vote a échoué',
  },
  getVotesError: {
    errno: 201,
    text: 'Impossible de récupérer les votes',
  },
}

module.exports = errors
