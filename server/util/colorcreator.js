const colors = {
  red: '#F56565',
  orange: '#ED8936',
  yellow: '#ECC94B',
  green: '#48BB78',
  teal: '#38B2AC',
  blue: '#4299E1',
  indigo: '#667EEA',
  purple: '#9F7AEA',
  pink: '#ED64A6',
}

const getRandomColor = () => {
  const keys = Object.keys(colors)
  const colorIndex = Math.floor(Math.random() * keys.length)
  return colors[keys[colorIndex]]
}

module.exports = getRandomColor
