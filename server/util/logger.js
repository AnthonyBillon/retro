const { createLogger, format, transports } = require('winston')
require('colors')

const {
  combine, timestamp, prettyPrint, printf,
} = format

const loggerFormat = printf((log) => {
  let levelTag
  switch (log.level) {
    case 'error':
      levelTag = log.level.red
      break
    case 'warn':
      levelTag = log.level.yellow
      break
    case 'info':
      levelTag = log.level.blue
      break
    case 'verbose':
      levelTag = log.level
      break
    case 'debug':
      levelTag = log.level
      break
    case 'silly':
      levelTag = log.level
      break
    default:
      levelTag = log.level
  }
  return `${log.timestamp} [${levelTag}] : ${log.message}`
})

const level = process.env.NODE_ENV !== 'production' ? 'debug' : 'info'

const logger = createLogger({
  format: combine(
    timestamp(),
    prettyPrint(),
  ),
  level,
  transports: [
    new transports.File({ filename: 'erdak.log' }),
  ],
})

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console({
    format: combine(
      timestamp(),
      loggerFormat,
    ),
  }))
}

module.exports = logger
