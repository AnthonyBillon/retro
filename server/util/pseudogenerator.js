const personnages = [
  {
    nom: 'Artémis',
    sexe: 'f',
  },
  {
    nom: 'Asclépios',
    sexe: 'm',
  },
  {
    nom: 'Athéna',
    sexe: 'f',
  },
  {
    nom: 'Déméter',
    sexe: 'f',
  },
  {
    nom: 'Dionysos',
    sexe: 'm',
  },
  {
    nom: 'Dolos',
    sexe: 'm',
  },
  {
    nom: 'Eole',
    sexe: 'm',
  },
  {
    nom: 'Éos',
    sexe: 'f',
  },
  {
    nom: 'Eris',
    sexe: 'f',
  },
  {
    nom: 'Géras',
    sexe: 'm',
  },
  {
    nom: 'Hades',
    sexe: 'm',
  },
  {
    nom: 'Hébé',
    sexe: 'f',
  },
  {
    nom: 'Hécate',
    sexe: 'f',
  },
  {
    nom: 'Hélios',
    sexe: 'm',
  },
  {
    nom: 'Héphaïstos',
    sexe: 'm',
  },
  {
    nom: 'Héra',
    sexe: 'f',
  },
  {
    nom: 'Hermès',
    sexe: 'm',
  },
  {
    nom: 'Hestia',
    sexe: 'f',
  },
  {
    nom: 'Hypnos',
    sexe: 'm',
  },
  {
    nom: 'Ilithye',
    sexe: 'f',
  },
  {
    nom: 'Léto',
    sexe: 'f',
  },
  {
    nom: 'Lyssa',
    sexe: 'f',
  },
  {
    nom: 'Momos',
    sexe: 'm',
  },
  {
    nom: 'Moros',
    sexe: 'm',
  },
  {
    nom: 'Némésis',
    sexe: 'f',
  },
  {
    nom: 'Oizès',
    sexe: 'f',
  },
  {
    nom: 'Pan',
    sexe: 'm',
  },
  {
    nom: 'Perséphone',
    sexe: 'f',
  },
  {
    nom: 'Poséïdon',
    sexe: 'm',
  },
  {
    nom: 'Séléné',
    sexe: 'f',
  },
  {
    nom: 'Thanatos',
    sexe: 'm',
  },
  {
    nom: 'Zeus',
    sexe: 'm',
  },
]


const adjectifs = [
  {
    m: 'pensant',
    f: 'pensante',
  },
  {
    m: 'en colère',
    f: 'en colère',
  },
  {
    m: 'motivé',
    f: 'motivée',
  },
  {
    m: 'heureux',
    f: 'heureuse',
  },
  {
    m: 'chanceux',
    f: 'chanceuse',
  },
  {
    m: 'serein',
    f: 'sereine',
  },
  {
    m: 'optimiste',
    f: 'optimiste',
  },
  {
    m: 'confiant',
    f: 'confiante',
  },
  {
    m: 'inspiré',
    f: 'inspirée',
  },
  {
    m: 'stupéfait',
    f: 'stupéfaite',
  },
  {
    m: 'soulagé',
    f: 'soulagée',
  },
  {
    m: 'rassasié',
    f: 'rassasiée',
  },
  {
    m: 'nerveux',
    f: 'nerveuse',
  },
]

const getRandomPseudo = () => {
  const personnage = personnages[Math.floor(Math.random() * personnages.length)]
  const adjectif = adjectifs[Math.floor(Math.random() * adjectifs.length)]
  if (personnage.sexe === 'f') {
    return `${personnage.nom} ${adjectif.f}`
  }
  return `${personnage.nom} ${adjectif.m}`
}

module.exports = getRandomPseudo
