const { Pool } = require('pg')
const logger = require('../util/logger')


const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST || 'localhost',
  database: process.env.DB_NAME,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT || 5432,
})


const createSession = async (code) => {
  await pool.query({
    text:
        'insert into votesession values(default, $1)',
    values: [code],
  })
}


const connectSession = async (color, sessionId, voteId, pseudo) => {
  await pool.query({
    text:
                'insert into vote values(default, $1, null, null, null, null, $2, $3, $4)',
    values: [sessionId, color, voteId, pseudo],
  })
}

const getSessionIdByCode = async (code) => pool.query({
  text: 'select votesession.id from votesession where code = $1',
  values: [code],
})

const addVote = async (sessionId, {
  R, D, A, Q,
}) => {
  await pool.query({
    text:
            'update vote '
            + 'set r=$1, d=$2, a=$3, q=$4 '
            + 'where voteid = $5',
    values: [R, D, A, Q, sessionId],
  })
}

const getVotes = async (code) => pool.query({
  text:
        'select couleur, r, d, a, q, voteid, pseudo '
        + 'from vote v '
        + 'inner join votesession s on v.votesession_id = s.id '
        + 'where s.code = $1 '
        + 'and r is not null '
        + 'and d is not null '
        + 'and a is not null '
        + 'and q is not null',
  values: [code],
})

const getVote = async (voteId) => pool.query({
  text:
        'select couleur, r, d, a, q, voteid, pseudo '
        + 'from vote v '
        + 'where v.voteid=$1',
  values: [voteId],
})

module.exports = {
  createSession,
  getVotes,
  addVote,
  connectSession,
  getSessionIdByCode,
  getVote,
}
