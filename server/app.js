const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const DIST_DIR = path.join(__dirname, '../dist/')

const connexionRouter = require('./routes/connexion')
const votesRouter = require('./routes/votes')

const app = express()

const io = require('./io/io')

app.io = io


app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/connexion', connexionRouter)
app.use('/votes', votesRouter)
app.use(express.static(DIST_DIR))
app.get('*', (request, response) => {
  response.sendFile(path.resolve(__dirname, '../dist', 'index.html'))
})

module.exports = app
