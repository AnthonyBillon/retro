const voteService = require('../services/vote')

const votes = async (req, res) => {
  try {
    const votesClient = await voteService.getVotes(req.decoded.code)
    res.send(votesClient)
  } catch (e) {
    res.status(400).send(e)
  }
}

module.exports = { votes }
