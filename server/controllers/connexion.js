const voteService = require('../services/vote')

const connexion = async (req, res) => {
  try {
    const connexionClient = await voteService.connectSession(req.body.code)
    res.send(connexionClient)
  } catch (e) {
    res.status(400).send(e)
  }
}

const createSession = async (req, res) => {
  try {
    const session = await voteService.createSession()
    res.send(session)
  } catch (e) {
    res.status(400).send(e)
  }
}

module.exports = { connexion, createSession }
