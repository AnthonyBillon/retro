FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./

COPY . .

RUN ./build-erdak.sh

ENV NODE_ENV production

EXPOSE 3000

ENTRYPOINT ["node", "./server/bin/www"]