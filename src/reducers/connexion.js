export default (state = {
  code: window.localStorage.getItem('code'),
  role: window.localStorage.getItem('role'),
  color: window.localStorage.getItem('color'),
  pseudo: window.localStorage.getItem('pseudo'),
  loading: false,
  error: false,
}, action) => {
  switch (action.type) {
    case 'POST_CREATE_SESSION_STARTED':
      return { ...state, loading: true }
    case 'POST_CREATE_SESSION_SUCCESS':
      return {
        ...state,
        role: 'host',
        code: action.code,
        error: false,
        loading: false,
      }
    case 'POST_CREATE_SESSION_ERROR':
      return { ...state, error: true }
    case 'POST_JOIN_SESSION_STARTED':
      return { ...state, loading: true }
    case 'POST_JOIN_SESSION_SUCCESS':
      return {
        ...state,
        role: 'client',
        code: action.code,
        error: false,
        loading: false,
        color: action.color,
        pseudo: action.pseudo,
      }
    case 'POST_JOIN_SESSION_ERROR':
      return { ...state, error: true }
    case 'DECONNECTER':
      return {
        code: undefined,
        role: undefined,
        loading: false,
        error: false,
      }
    default:
      return state
  }
}
