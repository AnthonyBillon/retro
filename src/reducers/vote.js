import { normalize, removeDuplicates } from '../util/normalizedata'

export default (state = {
  datas: [],
  monvote: {
    r: 0,
    d: 0,
    a: 0,
    q: 0,
  },
}, action) => {
  let datas
  switch (action.type) {
    case 'ADD_DATA':
      datas = state.datas
      datas = datas.filter((data) => data.voteid !== action.voteid)
      return {
        ...state,
        datas: [...datas, {
          pseudo: action.pseudo,
          voteid: action.voteid,
          data: {
            R: action.R,
            D: action.D,
            A: action.A,
            Q: action.Q,
          },
          meta: {
            color: action.couleur,
          },
        }],
      }
    case 'GET_VOTES_SUCCESS':
      datas = state.datas
      datas = removeDuplicates(datas, action.data)
      datas = [...datas, ...normalize(action.data)]
      return {
        ...state,
        datas,
      }
    case 'CLEAR_VOTES':
      return {
        datas: [],
        monvote: {
          r: 0,
          d: 0,
          a: 0,
          q: 0,
        },
      }
    case 'ADD_MONVOTE':
      return {
        ...state,
        monvote: {
          r: action.R,
          d: action.D,
          a: action.A,
          q: action.Q,
        },
      }
    default:
      return state
  }
}
