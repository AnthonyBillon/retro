export default (state = {
  error: undefined,
  showError: false,
}, action) => {
  switch (action.type) {
    case 'ADD_ERROR':
      return {
        ...state,
        error: action.error,
        showError: true,
      }
    case 'REMOVE_ERROR':
      return {
        ...state,
        showError: false,
      }
    default:
      return state
  }
}
