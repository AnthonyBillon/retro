import openSocket from 'socket.io-client'
import { addVote } from '../actions/vote'

const sck = openSocket()

let store

export const configureSocket = (st) => {
  store = st
  sck.on('ADD_DATA', ({
    R, D, A, Q, couleur, voteid, pseudo,
  }) => {
    store.dispatch(addVote(R, D, A, Q, couleur, voteid, pseudo))
  })
}

export const joinSessionRoom = () => {
  sck.emit('join', window.localStorage.getItem('code'))
}

export const leaveVote = () => {
  sck.emit('leave')
}
