import axios from 'axios'

const instance = axios.create({
  headers: {
    'Content-Type': 'application/json',
  },
  transformRequest: [(data, headers) => {
    const jwt = window.localStorage.getItem('jwt')
    if (jwt) {
      headers.authorization = jwt // eslint-disable-line no-param-reassign
    }
    return JSON.stringify(data)
  }],
})

export default instance
