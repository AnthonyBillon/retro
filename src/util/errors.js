export default (e) => {
  if (!e.response) {
    return {
      errno: 'inconnue',
      text: e.message,
    }
  }
  return e.response.data
}
