export const normalize = (datas) => (
  datas.map((d) => ({
    voteid: d.voteid,
    pseudo: d.pseudo,
    data: {
      R: d.r,
      D: d.d,
      A: d.a,
      Q: d.q,
    },
    meta: {
      color: d.couleur,
    },
  }))
)


export const removeDuplicates = (oldData, data) => {
  for (let i = 0; i < data.length; i += 1) {
    oldData.filter((d) => d.voteid !== data[i].voteid)
  }
  return oldData
}
