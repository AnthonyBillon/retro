import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { ReactReduxContext, useSelector } from 'react-redux'
import Header from '../components/Header'
import VoteReport from '../components/VoteReport'
import Connexion from '../components/Connexion'
import { joinSessionRoom, configureSocket } from '../api/socketio'

import { addVotesFromServer } from '../actions/vote'
import Toast from '../components/Toast'
import Vote from '../components/Vote'

export default () => {
  const { error, showError } = useSelector((state) => state.errors)

  return (
    <ReactReduxContext.Consumer>
      {({ store }) => {
        configureSocket(store)
        const voteid = window.localStorage.getItem('voteid')
        if (window.localStorage.getItem('code') && voteid) {
          joinSessionRoom()
          store.dispatch(addVotesFromServer(voteid))
        }
        return (
          <BrowserRouter>
            <div>
              <Header />
              <Switch>
                <Route path="/" exact component={Connexion} />
                <Route path="/vote" component={Vote} />
                <Route path="/chart" component={VoteReport} />
              </Switch>
              <Toast
                title={error ? `Erreur ${error.errno}` : ''}
                message={error ? error.text : ''}
                show={showError}
              />
            </div>
          </BrowserRouter>
        )
      }}

    </ReactReduxContext.Consumer>
  )
}
