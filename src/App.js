import React from 'react'
import ReactDOM from 'react-dom'
import './styles/tailwind.src.css'
import './fonts/Orkney_Regular.ttf'
import './fonts/Orkney_Bold.ttf'
import './fonts/Orkney_Regular_Italic.ttf'
import './fonts/Orkney_Bold_Italic.ttf'
import './fonts/Orkney_Light.ttf'
import './fonts/Orkney_Light_Italic.ttf'
import './fonts/Orkney_Medium.ttf'
import './fonts/Orkney_Medium_Italic.ttf'
import './images/logo.svg'
import { Provider } from 'react-redux'
import AppRouter from './routers/AppRouter'
import store from './store/configureStore'

const Index = () => (
  <Provider store={store}>
    <AppRouter />
  </Provider>
)


ReactDOM.render(<Index />, document.getElementById('root'))
