import axios from '../api/api'
import { addError } from './errors'
import createError from '../util/errors'

export const addVote = (R, D, A, Q, couleur, voteid, pseudo) => ({
  type: 'ADD_DATA',
  couleur,
  voteid,
  pseudo,
  R,
  D,
  A,
  Q,
})

export const addMonVote = (R, D, A, Q) => ({
  type: 'ADD_MONVOTE',
  R,
  D,
  A,
  Q,
})

export const addVotesFromServer = (voteid) => async (dispatch) => {
  dispatch({
    type: 'GET_VOTES_STARTED',
  })

  try {
    const res = await axios.get('/votes')
    dispatch({
      type: 'GET_VOTES_SUCCESS',
      data: res.data,
    })

    const monvote = res
      .data
      .filter((vote) => vote.voteid === voteid)
    if (monvote.length === 1) {
      dispatch(addMonVote(monvote[0].r, monvote[0].d, monvote[0].a, monvote[0].q))
    }
  } catch (e) {
    dispatch(addError(createError(e)))
  }
}

export const clearVotes = () => {
  window.localStorage.removeItem('jwt')
  window.localStorage.removeItem('code')
  window.localStorage.removeItem('role')
  return {
    type: 'CLEAR_VOTES',
  }
}
