import axios from '../api/api'
import { joinSessionRoom, leaveVote } from '../api/socketio'
import { addError } from './errors'
import createError from '../util/errors'

export const createSession = () => async (dispatch) => {
  dispatch({
    type: 'POST_CREATE_SESSION_STARTED',
  })

  try {
    const res = await axios.post('/connexion/create')
    window.localStorage.setItem('jwt', res.data.jwt)
    window.localStorage.setItem('code', res.data.code)
    window.localStorage.setItem('role', 'host')
    joinSessionRoom()
    dispatch({
      type: 'POST_CREATE_SESSION_SUCCESS',
      code: res.data.code,
    })
  } catch (e) {
    dispatch(addError(createError(e)))
  }
}

export const joinSession = (code) => async (dispatch) => {
  dispatch({
    type: 'POST_JOIN_SESSION_STARTED',
  })

  try {
    const res = await axios.post('/connexion/', {
      code,
    })
    window.localStorage.setItem('jwt', res.data.jwt)
    window.localStorage.setItem('code', code)
    window.localStorage.setItem('role', 'client')
    window.localStorage.setItem('color', res.data.color)
    window.localStorage.setItem('pseudo', res.data.pseudo)
    window.localStorage.setItem('voteid', res.data.voteid)
    joinSessionRoom()
    dispatch({
      type: 'POST_JOIN_SESSION_SUCCESS',
      code,
      color: res.data.color,
      pseudo: res.data.pseudo,
    })
    axios.get('/votes')
      .then((resVotes) => {
        dispatch({
          type: 'GET_VOTES_SUCCESS',
          data: resVotes.data,
        })
      })
  } catch (e) {
    dispatch(addError(createError(e)))
  }
}

export const deconnecter = () => {
  window.localStorage.removeItem('jwt')
  window.localStorage.removeItem('code')
  window.localStorage.removeItem('role')
  window.localStorage.removeItem('color')
  window.localStorage.removeItem('pseudo')
  window.localStorage.removeItem('voteid')
  leaveVote()
  return {
    type: 'DECONNECTER',
  }
}
