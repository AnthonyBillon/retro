export const removeError = () => ({
  type: 'REMOVE_ERROR',
})

export const addError = (error) => (dispatch) => {
  dispatch({
    type: 'ADD_ERROR',
    error,
  })
  setTimeout(() => {
    dispatch(removeError())
  }, 5000)
}
