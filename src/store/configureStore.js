import {
  applyMiddleware, combineReducers, compose, createStore,
} from 'redux'
import thunk from 'redux-thunk'
import chart from '../reducers/vote'
import connexion from '../reducers/connexion'
import errors from '../reducers/errors'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  combineReducers({
    chart,
    connexion,
    errors,
  }),
  composeEnhancers(applyMiddleware(thunk)),
)

export default store
