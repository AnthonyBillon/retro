import React from 'react'
import { useState } from 'react/cjs/react.production.min'

const Votebar = ({ text, onVote }) => {
  const [selected, setSelected] = useState(undefined)

  const onValueClick = (val) => {
    onVote(val)
    setSelected(val)
  }

  return (
    <div className="m-auto">
      <p className="text-4xl font-bold">{text}</p>
      <button
        type="button"
        className={`bg-gray-200 hover:bg-gray-400 text-2xl text-gray-800 font-bold py-2 px-4 rounded-l ${selected === 1 ? 'bg-gray-500' : ''}`}
        onClick={(ev) => {
          onValueClick(1, ev)
        }}
      >
        1
      </button>
      <button
        type="button"
        className={`bg-gray-200 hover:bg-gray-400 text-2xl text-gray-800 font-bold py-2 px-4 ${selected === 2 ? 'bg-gray-500' : ''}`}
        onClick={(ev) => {
          onValueClick(2, ev)
        }}
      >
        2
      </button>
      <button
        type="button"
        className={`bg-gray-200 hover:bg-gray-400 text-2xl text-gray-800 font-bold py-2 px-4 ${selected === 3 ? 'bg-gray-500' : ''}`}
        onClick={(ev) => {
          onValueClick(3, ev)
        }}
      >
        3
      </button>
      <button
        type="button"
        className={`bg-gray-200 hover:bg-gray-400 text-2xl text-gray-800 font-bold py-2 px-4 ${selected === 4 ? 'bg-gray-500' : ''}`}
        onClick={(ev) => {
          onValueClick(4, ev)
        }}
      >
        4
      </button>
      <button
        type="button"
        className={`bg-gray-200 hover:bg-gray-400 text-2xl text-gray-800 font-bold py-2 px-4 rounded-r ${selected === 5 ? 'bg-gray-500' : ''}`}
        onClick={(ev) => {
          onValueClick(5, ev)
        }}
      >
        5
      </button>
    </div>
  )
}

export default Votebar
