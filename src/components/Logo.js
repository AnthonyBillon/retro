import * as React from 'react'

const Logo = (props) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <svg height="40px" viewBox="0 0 553.72 176.38" {...props}>
    <defs>
      <linearGradient
        id="prefix__a"
        x1={22.05}
        y1={126.38}
        x2={154.34}
        y2={50}
        gradientTransform="rotate(-45 88.19 88.19)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#96c" />
        <stop offset={0.16} stopColor="#906ec7" />
        <stop offset={0.43} stopColor="#7683b9" />
        <stop offset={0.77} stopColor="#4da5a2" />
        <stop offset={1} stopColor="#2ebf91" />
      </linearGradient>
      <linearGradient
        id="prefix__b"
        x1={55}
        y1={116.25}
        x2={105.65}
        y2={46.54}
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#c94b4b" />
        <stop offset={0.14} stopColor="#c0474b" />
        <stop offset={0.38} stopColor="#a63c4c" />
        <stop offset={0.68} stopColor="#7d294d" />
        <stop offset={0.99} stopColor="#4b134f" />
      </linearGradient>
    </defs>
    <title>kadre à l&apos;envers</title>
    <g data-name="Calque 2">
      <g data-name="Calque 1">
        <path
          transform="rotate(45 88.19 88.19)"
          fill="url(#prefix__a)"
          d="M25.83 25.83h124.72v124.72H25.83z"
        />
        <text
          transform="translate(206.39 124.78)"
          fontSize={111}
          fill="#3d4852"
          fontFamily="Orkney-Bold,Orkney"
          fontWeight={700}
        >
          E
          <tspan x={59.94} y={0} letterSpacing={0}>
            r
          </tspan>
          <tspan x={108.56} y={0}>
            dak.
          </tspan>
        </text>
        <path
          fill="url(#prefix__b)"
          d="M134.39 88.19l-46.2-54.33-71.8 54.33 71.8 46.67 46.2-46.67z"
        />
      </g>
    </g>
  </svg>
)

export default Logo
