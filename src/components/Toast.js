import React from 'react'

const Toast = ({ show, title, message }) => (
  <div className={`flex toast ${show ? 'on-screen' : ''}`}>
    <div>
      <div className="bg-white rounded-lg border-gray-300 border p-3 shadow-lg">
        <div className="flex flex-row">
          <div className="px-2">
            <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
              <path d="M0 0h24v24H0V0z" fill="none" />
              <path
                fill="red"
                d="M11 15h2v2h-2zm0-8h2v6h-2zm.99-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"
              />
            </svg>
          </div>
          <div className="ml-2 mr-6">
            <span className="font-semibold">{title}</span>
            <span className="block text-gray-500">{message}</span>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Toast
