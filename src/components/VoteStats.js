import React from 'react'
import connect from 'react-redux/es/connect/connect'
import { useSelector } from 'react-redux'

const VoteStats = () => {
  const { datas, monvote } = useSelector((state) => state.chart)
  const { role } = useSelector((state) => state.connexion)

  const computeStat = (key) => {
    let sum = 0
    if (datas.length === 0) return 0

    for (let i = 0; i < datas.length; i += 1) {
      sum += datas[i].data[key]
    }
    return (sum / datas.length).toFixed(2)
  }

  return (
    <div className="bg-gray-900 text-white p-4 rounded-lg shadow-md hover:bg-gray-800">

      <h2 className="font-bold text-xl mb-2">Statistiques : </h2>
      <div className="flex">
        <div className="flex-1 font-bold">Critère</div>
        <div className="font-bold flex-1">Moyenne</div>
        {role === 'client' && <div className="font-bold flex-1">Moi</div>}
      </div>
      <div className="flex">
        <div className="flex-1">Résultat :</div>
        <div className="flex-1">{computeStat('R')}</div>
        {role === 'client' && <div className="flex-1">{monvote.r}</div>}
      </div>
      <div className="flex">
        <div className="flex-1">Déroulement :</div>
        <div className="flex-1">{computeStat('D')}</div>
        {role === 'client' && <div className="flex-1">{monvote.d}</div>}
      </div>
      <div className="flex">
        <div className="flex-1">Ambiance :</div>
        <div className="flex-1">{computeStat('A')}</div>
        {role === 'client' && <div className="flex-1">{monvote.a}</div>}
      </div>
      <div className="flex">
        <div className="flex-1">Qualité :</div>
        <div className="flex-1">{computeStat('Q')}</div>
        {role === 'client' && <div className="flex-1">{monvote.q}</div>}
      </div>
    </div>
  )
}

export default VoteStats
