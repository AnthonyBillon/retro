import React from 'react'
import { useSelector } from 'react-redux'
import VoteStats from './VoteStats'
import Chart from './Chart'

const VoteReport = () => {
  const { color, pseudo } = useSelector((state) => state.connexion)

  return (
    <div className="base-report flex flex-wrap -mx-3 overflow-hidden sm:-mx-3 lg:-mx-3 xl:-mx-3">
      <div
        className="my-3 px-3 w-full overflow-hidden sm:my-3 sm:px-3 sm:w-1/2 md:w-1/2 lg:my-3 lg:px-3 lg:w-2/3 xl:my-3 xl:px-3 xl:w-2/3"
      >
        <Chart />
      </div>
      <div
        className="px-3 w-full overflow-hidden sm:my-3 sm:px-3 sm:w-1/2 md:w-1/2 md:my-auto lg:my-auto lg:px-3 lg:w-1/3 xl:my-auto xl:px-3 xl:w-1/3 m-auto overflow-visible"
      >
        <div className="mx-8">
          <VoteStats />
          {pseudo && (
            <div className="text-2xl my-2">
              Vous êtes
              <span style={{ color }}>
                {' '}
                {pseudo}
              </span>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default VoteReport
