import React from 'react'
import { useSelector } from 'react-redux'
import { Radar } from 'react-chartjs-2'

export const Chart = () => {
  const { datas } = useSelector((state) => state.chart)

  const normalizeValues = () => {
    const datasets = (datas.map((data) => ({
      label: data.pseudo,
      data: [data.data.R, data.data.D, data.data.A, data.data.Q],
      borderColor: data.meta.color,
      backgroundColor: `${data.meta.color}00`,
    })))
    return {
      labels: ['R', 'D', 'A', 'Q'],
      datasets,
    }
  }

  return (
    <div className="">
      <Radar
        data={normalizeValues()}
        options={{
          legend: {
            display: false,
          },
          scale: {
            ticks: {
              beginAtZero: true,
              max: 5,
              min: 0,
              stepSize: 1,
            },
          },
          tooltips: {
            caretSize: 0,
            callbacks: {
              label: (tooltipItem, data) => {
                let label = data.datasets[tooltipItem.datasetIndex].label || ''

                if (label) {
                  label += ': '
                }
                label += Math.round(tooltipItem.yLabel)
                return label
              },
              title(tooltipItem) {
                return Math.round(tooltipItem.yLabel)
              },
            },
          },
        }}
      />
    </div>
  )
}

export default Chart
