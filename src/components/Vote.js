import React from 'react'
import { useDispatch } from 'react-redux'
import Lottie from 'react-lottie'
import { useState } from 'react/cjs/react.production.min'
import Votebar from './Votebar'
import * as notificationAnimation from '../animations/confirmation.json'
import { addMonVote } from '../actions/vote'

export default () => {
  const dispatch = useDispatch()

  const [isVoteComplete, setIsVoteComplete] = useState(false)
  const voteValues = {}

  const defaultOptions = {
    loop: false,
    autoPlay: false,
    animationData: notificationAnimation.default,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  }

  const addVoteTo = (val, key) => {
    voteValues[key] = val
    if (voteValues.R && voteValues.D && voteValues.A && voteValues.Q) {
      dispatch(addMonVote(voteValues.R, voteValues.D, voteValues.A, voteValues.Q))
      setIsVoteComplete(true)
    }
  }

  return (
    <div>
      {isVoteComplete
        ? (
          <div className="xl:w-1/3 lg:w-1/3 md:w-1/2 sm:w-full w-full mx-auto">
            <Lottie options={defaultOptions} />
          </div>
        )
        : (
          <div className="container mx-auto pt-12 md:pt-8 flex flex-col content-center">
            <Votebar
              text="Résultats"
              onVote={(val) => {
                addVoteTo(val, 'R')
              }}
            />
            <Votebar
              text="Déroulement"
              onVote={(val) => {
                addVoteTo(val, 'D')
              }}
            />
            <Votebar
              text="Ambiance"
              onVote={(val) => {
                addVoteTo(val, 'A')
              }}
            />
            <Votebar
              text="Qualité"
              onVote={(val) => {
                addVoteTo(val, 'Q')
              }}
            />
          </div>
        )}
    </div>
  )
}
