import React from 'react'
import { Link } from 'react-router-dom'
import { connect, useSelector } from 'react-redux'
import Logo from './Logo'

const Header = () => {
  const connexion = useSelector((state) => state.connexion)
  return (
    <header
      className="flex bg-white border-b border-gray-200 top-0 inset-x-0 z-100 h-16 items-center bg-background-ternary"
    >
      <Link to="/">
        <Logo />
      </Link>
      {connexion.role === 'client' && (
      <Link
        className="px-2 py-1 transition duration-200 ease-in-out relative block hover:translate-x-2px hover:text-gray-900 text-gray-600 font-medium"
        to="/vote"
      >
        Voter
      </Link>
      )}
      {connexion.role && (
      <Link
        className="px-2 py-1 transition duration-200 ease-in-out relative block hover:translate-x-2px hover:text-gray-900 text-gray-600 font-medium"
        to="/chart"
      >
        Résultats
      </Link>
      )}
    </header>
  )
}

export default Header
