import React from 'react'
import { connect, useDispatch, useSelector } from 'react-redux'
import ConnexionForm from './ConnexionForm'
import { deconnecter } from '../actions/connexion'
import { clearVotes } from '../actions/vote'

const Connexion = () => {
  const dispatch = useDispatch()
  const { code, color, pseudo } = useSelector((state) => state.connexion)

  return (
    <div className="container mx-auto pt-12 md:pt-8 flex flex-col">
      <div className="mx-4">
        <h1 className="main-title text-6xl font-bold">Erdak</h1>
        <p className="text-3xl">Qu&apos;avez-vous pensé de ce sprint ?</p>
      </div>

      {code
        ? (
          <div className="text-center container mx-auto pt-12 md:pt-8 ">
            <div className="text-2xl">Code de la session :</div>
            <div className="text-6xl font-bold">{code}</div>
            {pseudo && (
              <div className="text-3xl">
                Vous êtes
                <span style={{ color }}>
                  {' '}
                  {pseudo}
                </span>
              </div>
            )}

            <button
              type="button"
              className="rounded-lg px-4 md:px-5 xl:px-4 py-3 md:py-4 xl:py-3 bg-indigo-500 hover:bg-indigo-600 md:text-lg xl:text-base text-white font-semibold leading-tight shadow-md"
              onClick={() => {
                dispatch(deconnecter())
                dispatch(clearVotes())
              }}
            >
              Se déconnecter
            </button>
          </div>
        )
        : <ConnexionForm />}
    </div>
  )
}

export default Connexion
