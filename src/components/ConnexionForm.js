import React from 'react'
import { useDispatch } from 'react-redux'
import { createSession, joinSession } from '../actions/connexion'

const ConnexionForm = () => {
  const dispatch = useDispatch()

  const onSubmitConnexion = (ev) => {
    ev.preventDefault()
    dispatch(joinSession(ev.target.code.value))
  }

  return (
    <div className="flex flex-wrap overflow-hidden lg:-mx-4 xl:-mx-4 text-indigo-600">
      <div
        className="my-4 px-4 w-full overflow-hidden sm:my-3 sm:px-3 sm:w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/2 xl:my-4 xl:px-4 xl:w-1/2 overflow-visible"
      >
        <button
          type="button"
          className="rounded-lg px-4 md:px-5 xl:px-4 py-3 md:py-4 xl:py-3 bg-indigo-500 hover:bg-indigo-600 md:text-lg xl:text-base text-white font-semibold leading-tight shadow-md w-full"
          onClick={() => {
            dispatch(createSession())
          }}
        >
          Créer un vote
        </button>
      </div>
      <div
        className="my-4 px-4 w-full overflow-visible sm:my-3 sm:px-3 sm:w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/2 xl:my-4 xl:px-4 xl:w-1/2"
      >
        <form className="flex overflow-visible" action="" onSubmit={(ev) => onSubmitConnexion(ev)}>
          <input
            type="text"
            maxLength="6"
            placeholder="Code"
            name="code"
            className="rounded-l-lg flex-1 transition-colors duration-100 ease-in-out focus:outline-0 border border-transparent focus:bg-white focus:border-gray-300 placeholder-gray-600 bg-gray-200 py-2 pr-4 pl-10 block appearance-none leading-normal ds-input font-bold uppercase min-w-0"
          />
          <button type="submit" className="rounded-r-lg px-4 md:px-5 xl:px-4 py-3 md:py-4 xl:py-3 bg-indigo-500 hover:bg-indigo-600 md:text-lg xl:text-base text-white font-semibold leading-tight shadow-md">Se connecter</button>
        </form>
      </div>
    </div>
  )
}

export default ConnexionForm
