# Erdak 
![alt text](SVG/logo.svg "Title Text")

## Prérequis :
> :warning: les identifiants de la base de données ne sont plus configurés par défaut. Il est nécessaire de les placer soit dans un fichier .env (conseillé en developpement) soit dans les variables d'environnement

> :warning: depuis la version 1.1.0, les migrations s'effectuent au démarrage de l'application

L'application nécessite une base de donnée Postgres (>=10). Le fichier ```docker-compose.yml``` 

Les variables d'environnement suivantes doivent être configurées :

| Variable |	Description                     |	Défaut  | Requis |
| -------- | ---------------------------------- | --------- | :----: |
| DB_NAME  | Nom de la base de données          |		    | ✅    |
| DB_USER  | Utilisateur de la base de données  |		    | ✅    |
| DB_PASS  | Mot de passe de la base de données |	        | ✅    |    
| DB_HOST  | Adresse de Postgres                |           | ✅    |
| DB_PORT  | Port de Postgres                   |    	    | ✅    |
| PORT	   | Port HTTP de Erdak                 | 3000	    | ❌    |     

## Lancer l'application : 

```shell script
# Installer les dépendances : 
npm install
# Migration de la base de données
npm run migrate
# Lancement de l'application en mode production
npm start
# Lancer l'application en mode développement
npm run dev
```
 
## Docker :

L'application est prévue pour être utilisée avec Docker. L'image est disponible sur le registre du repo. 
Le fait que les migrations s'appliquent au démarrage est volontaire puisque c'est une petite application qui n'est pas destinée à être utilisée intensément (et donc on peut se permettre un down time). La bonne pratique consisterait à appliquer les migrations à partir d'un container d'initialisation. 

### Avec docker-compose : 
Exemple de docker-compose.yml
```yaml
version: '3.3'
services:
  postgres:
    image: postgres:latest
    environment:
      - POSTGRES_DB=${DB_NAME}
      - POSTGRES_USER=${DB_USER}
      - POSTGRES_PASSWORD=${DB_PASS}
    ports:
      - "${DB_PORT}:5432"
    restart: always
  erdak:
    image: erdak:latest
    environment:
      - DB_NAME=${DB_NAME}
      - DB_USER=${DB_USER}
      - DB_PASS=${DB_PASS}
      - DB_HOST=postgres
      - DB_PORT=5432
    ports:
      - "3000:3000"
    links:
      - "postgres:database"
    restart: always
    depends_on:
      - postgres
```

## TODO : 

- Clarifier le graphique des résultats
- Ajouter des tests unitaires
- Ajouter un mode sombre
- Corriger les erreurs de pattern dans le front (React)

## Issues et suggestion : 

En cas de problème ou pour faire une suggestion, créer une issue sur Gitlab 